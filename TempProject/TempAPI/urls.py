from django.urls import path
from .views import UserRegistration, UserDetailsAll, UserDetails

urlpatterns = [
    path('UserRegistration',UserRegistration.as_view()),
    path('UserDetailsAll',UserDetailsAll.as_view()),
    path('User/<str:UserId>', UserDetails.as_view()),

]
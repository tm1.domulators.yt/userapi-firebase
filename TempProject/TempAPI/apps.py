from django.apps import AppConfig


class TempapiConfig(AppConfig):
    name = 'TempAPI'

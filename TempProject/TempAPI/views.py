from django.shortcuts import render
from firebase import firebase
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from rest_framework import status
import jsonschema
from jsonschema import validate

firebase=firebase.FirebaseApplication("https://userdata-api.firebaseio.com/", None)

class UserRegistration(APIView):
    def post(self, request):
        userdata=request.data
        userSchema = {
            "UserFirstName":"string",
            "UserLastName": "string",
            "UserEmail": "string",
            "UserGender": "string",
            "UserMobileNumber": "number"
        }
        userdata["UserFirstName"]=userdata["UserFirstName"].lower()
        try:
            validate(instance=userdata, schema=userSchema)
        except jsonschema.exceptions.ValidationError as err:
            return Response({"Check your data"},status=status.HTTP_400_BAD_REQUEST)
        try:
            result=firebase.post('userdata-api/user-details/',userdata)
        except:
            return Response({"Check your link "},status=status.HTTP_400_BAD_REQUEST)
        return Response(result,status=status.HTTP_201_CREATED)

class UserDetailsAll(APIView):
    def get(self, requests):
        try:
            result=firebase.get('userdata-api/user-details/','')
        except:
            return Response({"Check your link "},status=status.HTTP_400_BAD_REQUEST)
        return Response(result)

class UserDetails(APIView):
    def get(self,request,UserId):
        try:
            result = firebase.get('userdata-api/user-details/', UserId)
        except:
            return Response({"Check your link "}, status=status.HTTP_400_BAD_REQUEST)
        return Response(result)

    def put(self, request, UserId):
        userdata=request.data
        url='userdata-api/user-details/'
        try:
            firebase.put(url=url, name=UserId, data=userdata)
        except:
            return Response({"Check your link "}, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_200_OK)

    def delete(self,request, UserId):
        url = 'userdata-api/user-details/'
        try:
            firebase.delete(url=url, name=UserId)
        except:
            return Response({"Check your link "}, status=status.HTTP_400_BAD_REQUEST)
        return Response({ "Deleted" }, status=status.HTTP_200_OK)

